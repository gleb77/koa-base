import db from '../../component/db';

const categoryWrite = db.model('write', 'category');

class CategoryWrite {

  findById(_id) {
    return categoryWrite.findRow({
      query: {
        _id,
        isDeleted: false,
      }
    });
  }

  findRow(parentCategory) {
    return categoryWrite.findRow({
      query: {
        parentCategory
      }
    });
  }

  getAll() {
    return categoryWrite.findRows({
      query: {
        isDeleted: false,
      }
    });
  }

  create({ parentCategory = null, title }) {
    const data = {
      title,
      parentCategory,
    };
    return categoryWrite.insertRow({ data });
  }

  delete(_id) {
    return categoryWrite.updateRow({
      query: {
        _id,
      },
      data: {
        updatedAt: Date.now(),
        isDeleted: true,
      }
    });
  }

  update( _id, data) {
    return categoryWrite.updateRow({
      query: {
        _id
      },
      data,
    });
  }

  updateChildCategory( newParent, currentParentCategory) {
    const data = {
      parentCategory: newParent,
      updatedAt: Date.now(),
    };

    return categoryWrite.updateRows({
      query: {
        currentParentCategory,
      },
      data,
    });
  }

}

export default new CategoryWrite();
