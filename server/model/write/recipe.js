import db from '../../component/db';

const recipeWrite = db.model('write', 'recipe');

class RecipeWrite {

  findRecipeById(_id) {
    return recipeWrite.findRow({
      query: {
        _id,
        isDeleted: false,
      }
    });
  }

  getAllRecipes({category}) {
    return recipeWrite.findRows({
      query: {
        category,
        isDeleted: false,
      }
    });
  }

  getOneById(_id) {
    return recipeWrite.findRows({
      query: {
        _id,
        isDeleted: false,
      }
    });
  }

  create({ title, description, category }) {
    return recipeWrite.insertRow({
      data: {
        title,
        description,
        category,
      },
    });
  }

  delete({_id}) {
    return recipeWrite.deleteRow({
      query: {
        _id
      }
    });
  }

  update(request, obj) {
    let data = {
      title: obj.title,
      description: obj.description,
      category: obj.category,
      updatedAt: Date.now(),
    };

    return recipeWrite.updateRow( {
      query: {
        _id: request
      },
      data
    });
  }

  updateCategory( newCategory, currentCategory) {
    const data = {
      category: newCategory,
      updatedAt: Date.now(),
    };

    return recipeWrite.updateRows({
      query: {
        category:currentCategory,
      },
      data,
    });
  }

}

export default new RecipeWrite();
