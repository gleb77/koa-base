import { Schema } from 'mongoose';
import * as _ from 'lodash';

import standardField from '../../component/db/dbStandardField';

export default new Schema(
  _.assignIn(
    _.cloneDeep(standardField),
    {
      title: {
        type: String,
        require: true,
      },
      description: {
        type: String,
        default: '',
        require: true,
      },
      category: {
        type: Schema.Types.ObjectId,
        require: true,
        ref: 'Category',
        default: null,
      },

    })
);
