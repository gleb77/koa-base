import koaRouter from 'koa-router';

import middlewareWrapper from '../component/middlewareWrapper';
import categoryAction from '../action/category';
import categoryValidate from '../validator/category';

export let router2 = koaRouter({
  prefix: '/api/v1/categories'
});

router2.post('/', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async() => {
    const reqData = await categoryValidate.create(ctx.request.body);
    return categoryAction.create(reqData);
  });
});

router2.get('/', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, () => {
    return categoryAction.getAll();
  });
});

router2.delete('/:id', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async () => {
    let reqData = await categoryValidate.delete(ctx.params.id);

    return categoryAction.delete(reqData);
  });
});

router2.put('/:id', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async() => {
    let reqData = await categoryValidate.update(ctx.params.id, ctx.request.body);
    return categoryAction.update(reqData, ctx.params.id);
  });
});


