import koaRouter from 'koa-router';

import middlewareWrapper from '../component/middlewareWrapper';
import recipeAction from '../action/recipe';
import recipeValidate from '../validator/recipe';

export let router = koaRouter({
  prefix: '/api/v1/recipes'
});

router.post('/', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async() => {
    const reqData = await recipeValidate.create(ctx.request.body);
    return recipeAction.create(reqData);
  });
});

router.get('/:id', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async () => {
    const data= {
      category: ctx.params.id
    };
    let reqData = await recipeValidate.getAllRecipes(data);
    return recipeAction.getAllRecipes(reqData);
  });
});

router.get('/recipe/:id', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async () => {
    const data= {
      _id: ctx.params.id
    };
    let reqData = await recipeValidate.getOneById(data);

    return recipeAction.getOneById(reqData);
  });
});

router.delete('/:id', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async () => {
    let reqData = await recipeValidate.delete(ctx.params.id);
    return recipeAction.delete(reqData);
  });
});

router.put('/:id', async(ctx) => {
  await middlewareWrapper.wrape(ctx, null, async() => {
    let reqData = await recipeValidate.update(ctx.params.id, ctx.request.body);
    return recipeAction.update(reqData, ctx.params.id);
  });
});



