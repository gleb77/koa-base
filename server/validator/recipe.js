import * as _ from 'lodash';

import categoryWrite from '../model/write/category';
import recipeWrite from '../model/write/recipe';

import validator from '../component/validator';

const recipeFreeData = [
  'title',
  'description',
  'category',
  '_id'
];

const validateMethod = (body) => {
  const fullValidateObj = {
    title: {
      notEmpty: {
        message: 'Valid title is required',
      },
    },
    description: {
      notEmpty: {
        message: 'Valid description is required',
      },
    },
    category: {
      notEmpty: {
        message: 'Valid category is required',
      },
    },
  };

  const fieldsList = [
    'title',
    'category',
    'description'
  ];

  const validateObj = {};

  for (const field of fieldsList) {
    if (!_.isUndefined(body[field]) && fullValidateObj[field]) {
      validateObj[field] = fullValidateObj[field];
    }
  }

  let errorList = validator.check(body, validateObj);

  if (errorList.length) {
    throw (errorList);
  }
};



const findCategoryById = async (date) => {
  if(!validator.validator.isMongoId(date.category)) {
    throw ([{ param: 'category', message: 'Category not found' }]);
  }

  const categoryObj = await categoryWrite.findById(date.category);
  if (!categoryObj) {
    throw ([{ param: 'category', message: 'Category not found' }]);
  }

  return categoryObj;
};

const findRecipeById = async (id) => {


  if(!validator.validator.isMongoId(id._id)) {
    throw ([{ param: '_id', message: 'Recipe not found+' }]);
  }
  const recipe = await recipeWrite.findRecipeById(id._id);

  if (!recipe) {
    throw ([{ param: '_id', message: 'Recipe not found-' }]);
  }

  return recipe;
};


async function checkParentId (list, toObj) {
  let breadCrumbs = [];
  let toIdItem = list.find(category => category._id.toString() === toObj._id.toString());
  breadCrumbs.unshift(toIdItem);
  while(toIdItem.parentCategory !== null) {
    toIdItem = list.find(category => category._id.toString() === toIdItem.parentCategory.toString());
    breadCrumbs.unshift(toIdItem);
  }
  console.log(breadCrumbs.join('/'));
  return breadCrumbs;
}

const getBreadCrumbs = async (toId) => {
  const toIdres = await recipeWrite.findRecipeById(toId);

  if (!toIdres) {
    throw ([{ param: 'parentCategory', message: 'Category not found' }]);
  }

  const categoryList = await categoryWrite.getAll();

  await checkParentId(categoryList, toIdres.category);
};

class RecipeValidate {

  async update(recipeId, body) {
    validateMethod(body);
    await findRecipeById(recipeId);

    return _.pick(body, recipeFreeData);
  }

  async delete(recipeId) {
    findRecipeById(recipeId);

    const body = {
      _id: recipeId
    };


    return _.pick(body, recipeFreeData);
  }


  async create(body) {
    await findCategoryById(body);
    validateMethod(body);

    return _.pick(body, recipeFreeData);
  }

  async getAllRecipes(body) {
    await findCategoryById(body);
    validateMethod(body);

    return _.pick(body, recipeFreeData);
  }

  async getOneById(body) {
    await findRecipeById(body);
    validateMethod(body);
    await getBreadCrumbs(body);
    return _.pick(body, recipeFreeData);
  }
}

export default new RecipeValidate();
