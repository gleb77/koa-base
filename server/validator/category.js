import * as _ from 'lodash';

import categoryWrite from '../model/write/category';

import validator from '../component/validator';

const categoryFreeData = [
  'title',
  'parentCategory',
  '_id'
];

const validateMethod = (body) => {

  const fullValidateObj = {
    title: {
      notEmpty: {
        message: 'Valid title is required',
      },
    },
  };

  const fieldsList = [
    'title',
    // 'parentCategory',
  ];

  const validateObj = {};

  for (const field of fieldsList) {
    if (!_.isUndefined(body[field]) && fullValidateObj[field]) {
      validateObj[field] = fullValidateObj[field];
    }
  }

  let errorList = validator.check(body, validateObj);

  if (errorList.length) {
    throw (errorList);
  }
};



const findCategoryId = async (id) => {
  if(!validator.validator.isMongoId(id)) {
    throw ([{ param: 'parentCategory', message: 'Category not found' }]);
  }
  const categoryObj = await categoryWrite.findById(id);
  if (!categoryObj) {
    throw ([{ param: 'parentCategory', message: 'Category not found' }]);
  }
  return categoryObj;
};


function checkParentId (list, fromObj, toObj) {
  let toIdItem = list.find(category => category._id.toString() === toObj._id.toString());
  while(toIdItem.parentCategory !== null) {
    toIdItem = list.find(category => category._id.toString() === toIdItem.parentCategory.toString());

    if(toIdItem._id.toString() === fromObj._id.toString()) {
      throw ('recursion');
    }
  }
}

const findCategoryIdforUpdate = async (fromId, toId) => {
  if(!validator.validator.isMongoId(fromId) && !validator.validator.isMongoId(toId)) {
    throw ([{ param: 'parentCategory', message: 'Category not found' }]);
  }

  const fromIdres = await categoryWrite.findById(fromId);
  const toIdres = await categoryWrite.findById(toId);

  if (!fromIdres && !toIdres) {
    throw ([{ param: 'parentCategory', message: 'Category not found' }]);
  }

  if(fromIdres._id.toString() === toIdres._id.toString()) {
    throw ([{ param: 'ids equal', message: 'Ids equal' }]);
  }

  const categoryList = await categoryWrite.getAll();

  checkParentId(categoryList, fromIdres, toIdres);
};

class CategoryValidate {
  async update(categoryId, body) {

   validateMethod(body);
   await findCategoryId(categoryId);

    if(body.parentCategory) {
      await findCategoryIdforUpdate(categoryId, body.parentCategory);
    }
    return _.pick(body, categoryFreeData);
  }

  async delete(categoryId) {
    findCategoryId(categoryId);



    const body = {
      _id: categoryId
    };

    return _.pick(body, categoryFreeData);
  }


  async create(body) {
    if (body.parentCategory) {
      await findCategoryId(body.parentCategory);
    }
    validateMethod(body);

    return _.pick(body, categoryFreeData);
  }

}

export default new CategoryValidate();
