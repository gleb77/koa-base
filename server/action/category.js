import categoryWrite from '../model/write/category';
import recipeWrite from '../model/write/recipe';

class CategoryAction {
  update(data, _id) {
    return categoryWrite.update(_id, data);
  }

  create(data) {
    return categoryWrite.create(data)
  }

  findById(id) {
    return categoryWrite.findById(id);
  }

  getAll() {
    return categoryWrite.getAll();
  }

  async delete(id) {
    const deleteItem = await categoryWrite.findById(id);
    const parentID = deleteItem.parentCategory;
    const deleteId = deleteItem._id;
    await categoryWrite.updateChildCategory(parentID, deleteId);

    await recipeWrite.updateCategory(parentID, deleteId);

    return categoryWrite.delete(id);
  }
}

export default new CategoryAction();
