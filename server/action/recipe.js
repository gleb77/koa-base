import recipeWrite from '../model/write/recipe';

class RecipeAction {
  update(data, _id) {
    return recipeWrite.update(_id, data);
  }

  create(data) {
    return recipeWrite.create(data);
  }

  findById(id) {
    return recipeWrite.findRecipeById(id);
  }

  getAllRecipes(category) {
    return recipeWrite.getAllRecipes(category);
  }

  getOneById(id) {
    return recipeWrite.getOneById(id);
  }

  delete(id) {
    return recipeWrite.delete(id);
  }
}

export default new RecipeAction();
